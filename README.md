# Introduction

This simple class implements in python the Werewolves game (see [wikipedia article](https://en.wikipedia.org/wiki/The_Werewolves_of_Millers_Hollow)). It is only aiming at helping the Game Master going through the game and keeping track of the different actions and rounds. It is *not* to be distributed among all players.

The implementation is very basic and allows up to 18 players. At the moment the only roles supported are peasants, werewolves, fortuneteller, little-girl and witch.

The mayor is not supported at the moment.

# Main functions

See next section for an example on how to run an instance of the game. This section explains few of the main functions useful to play a game.

* start_game()
    - This function allows you to start the game, enter the number of players, automatically assigning roles and initializing all the different initial statuses  
* play_round()
    -  This function is the main function. It runs the different roles in sequence. 
        + First the fortuneteller
        + Second the werewolves
        + Third the witch
        + Fourth the voting round
* printer functions: Series of functions that can help getting information about the status of the game
* fixer functions: Series of functions that can help you fix something in case a wrong entry was entered

# Test

    import importlib
    import GameInitiator
    import logging

    logger = logging.getLogger(__name__)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(formatter)
    logger.addHandler(stdout_handler)
    logger.setLevel(logging.DEBUG)
    libs_logger = logging.getLogger('GameInitiator')
    libs_logger.addHandler(stdout_handler)

    importlib.reload(GameInitiator)

    game_1 = GameInitiator.GameInitiator()
    game_1.start()

    game_1.print_players()
    game_1.start_new_round()

