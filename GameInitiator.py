import random, sys, collections, logging

logger = logging.getLogger(__name__)


# Static methods
def format_string(string, length):
    """
    This function takes a string and output only its first n letters if bigger than n
    or pad it with space if smaller than n
    :param string: input string
    :param length: output string length
    :return: output string
    """
    ls = len(string)
    if ls >= length:
        return string[:length]
    n_spaces = length - ls
    return string + n_spaces * ' '


def chose_from_list(list_possibilities):
    """
    Allow to choose from a list of items
    :param list_possibilities:
    :return one item:
    """

    nr_possibilities = len(list_possibilities)

    print('\tChose from the list below')
    for idx in range(nr_possibilities):
        print('\t\t[%d] %s' % (idx, list_possibilities[idx]))
    flag_valid = 0
    while flag_valid == 0:
        choice = input('\tYour choice:\t')
        try:
            choice_n = int(choice)
        except ValueError:
            print('\tExpect an integer')
            continue
        if choice_n < 0 or choice_n >= nr_possibilities:
            print('\tValue should be in 0-%d' % (nr_possibilities - 1))
            continue
        flag_valid = 1
    return list_possibilities[choice_n]


class GameInitiator():
    """
    This class is to initiate a werewolve game and randomly assign roles to players
    """

    MINPLAYERS = 9
    LISTROLES = ['Peasant',
                 'Peasant',
                 'Peasant',
                 'Peasant',
                 'Witch',
                 'Fortuneteller',
                 'Little-girl',
                 'Werewolf',
                 'Werewolf',  # 9
                 'Cupidon',
                 'Peasant',
                 'Werewolf',  # 12
                 'Peasant',
                 'Peasant',
                 'Peasant',
                 'Peasant',
                 'Peasant',
                 'Werewolf'  # 18
                 ]

    def __init__(self):
        self.number_players = 0
        self.players = {}
        self.list_available_roles = []
        self.witch_potions = {'life': 1, 'death': 1}
        self.witch_target_potion = {'life': None, 'death': None}
        self.werewolves_victims = {}
        self.votes = {}
        self.round = 0

    # Getters
    def _get_number_available_roles(self):
        return len(self.list_available_roles)

    def _get_number_players(self):
        return len(list(self.players.keys()))

    def _get_role(self):
        list_available_roles = self.list_available_roles
        if len(list_available_roles) == 0:
            print('All roles have already been assigned, start again with more players if needed')
            return None
        else:
            role = random.choice(list_available_roles)
            list_available_roles.remove(role)
            self.list_available_roles = list_available_roles
            return role

    def _get_player_names(self):
        return list(self.players.keys())

    def _get_round_iteration(self, round_value=None, iteration=None):
        if round_value is None:
            t_round = self.round
        else:
            t_round = round_value

        if iteration is None:
            t_iteration = max(self.votes[t_round].keys())
        else:
            t_iteration = iteration

        return t_round, t_iteration

    def _get_voted(self, round_value, iteration):
        t_round, t_iteration = self._get_round_iteration(round_value, iteration)
        voted = list(self.votes[t_round][t_iteration].values())
        voted_stats = collections.Counter(voted)
        return voted_stats

    def _get_voted_name(self, round_value=None, iteration=None):
        """
        For a given iteration of votes return the player(s) most voted against
        :param round_value: default current
        :param iteration: default current
        :return: list of most voted players
        """
        t_round, t_iteration = self._get_round_iteration(round_value, iteration)
        votes = self._get_voted(t_round, t_iteration)
        voted_counts = list(votes.values())
        voted_names = list(votes.keys())
        max_counts = max(list(votes.values()))
        most_voted = [_ for _ in voted_names if votes[_] == max_counts]
        return most_voted, max_counts

    def _get_witch_potions(self):
        potions = self.witch_potions
        left_potions = []
        for potion in potions.keys():
            if potions[potion] == 1:
                left_potions.append(potion)
        return left_potions

    def _get_werewolf_victim(self, value_round=None):
        if value_round is None:
            t_round = self.round
        else:
            t_round = value_round

        try:
            victim = self.werewolves_victims[t_round]
            return victim
        except KeyError:
            print('This turn was not run yet or does not exist')
            return None

    # Setters
    def _set_number_players(self):

        flag_valid = 0

        while flag_valid == 0:
            input_number_players = input('Enter the number of players (between 9 and 18):\t')
            try:
                number_players = int(input_number_players)
            except ValueError:
                print('Expected an integer')
                continue
            if number_players < 9 or number_players > 18:
                print('Value out of range')
                continue
            flag_valid = 1

        self.number_players = number_players

    def _set_available_roles(self):
        list_game_roles = self.LISTROLES[:self.number_players]
        self.list_available_roles = list_game_roles

    def _set_player_status(self, name, status='Dead'):
        """
        For a given name (if present in the players) set its status (Alive or Dead, default is Dead)
        :param name:
        :param status:
        :return:
        """
        if status not in ['Dead', 'Alive']:
            print('Not supported status')
        else:
            if name not in list(self.players.keys()):
                print('%s not present in the players')
            else:
                self.players[name][1] = status

    def _set_werewolf_victim(self, victim, value_round=None):

        victim_status = self.get_player_status(victim)

        if victim_status == 'Dead':
            print('%s is already dead' % victim)
            return 1

        if value_round is None:
            t_round = self.round
        else:
            t_round = value_round

        self._set_player_status(victim)
        self.werewolves_victims[t_round] = victim

    # Internal actions
    ## Game set up
    def _assign_role(self, name):
        list_names = list(self.players.keys())
        while name in list_names:
            name = input('Name %s already exists, enter different name:\t' % name)
        role = self._get_role()
        self.players[name] = [role, 'Alive']

    def _add_player(self):
        number_players = self._get_number_players()
        if number_players == self.number_players:
            print('Maximum number of players reached. Start a new game with different number of players if needed')
        else:
            name = input('Enter name:\t')
            self._assign_role(name)

    def _add_players(self):
        number_players = self._get_number_players()
        if number_players == self.number_players:
            print('Maximum number of players reached. Start a new game with different number of players if needed')
        else:
            names = input('Enter list of names, separated by spaces:\t')
            for name in names.split():
                number_players = self._get_number_players()
                if number_players == self.number_players:
                    print('All roles have been assigned, cannot add %s or any following player' % name)
                    print('Start a new game with more players if needed')
                    break
                else:
                    self._assign_role(name)

    ## Fortuneteller turn
    def _run_fortuneteller(self):
        """
        Run the round of the fortuneteller
            Check if fortuneteller is still alive
            Ask for the name of player
            Return the role of this player
        :return:
        """
        # Check if fortuneteller is still alive
        fortuneteller = self.get_players_with_role('Fortuneteller')
        if len(fortuneteller) == 0:
            print("\tFortuneteller seems to be dead")
            return 0
        print("\tFortuneteller is %s" % fortuneteller[0])
        flag_alive = 0
        while flag_alive == 0:
            print('\tTarget must be alive')
            name_target = input("\tEnter the name of player whose identity should be revealed:\t")
            status = self.get_player_status(name_target)
            flag_alive = int(status == 'Alive')

        role_target = self.get_player_role(name_target)
        print('\t%s is %s' % (name_target, role_target))

    ## Werewolves turn
    def _run_werewolves(self):
        """
        Run the round of the werewolves.
            Check if there are some still alive
            Display their names
            Enter their vote against a player
        :return nothing:
        """
        # check if there are werewolves alive
        werewolves = self.get_players_with_role('Werewolf')
        if len(werewolves) == 0:
            print('\tAll werewolves are dead')
            return 0

        print("\tThe werewolves still alive are:")
        for wolf in werewolves:
            print("\t\t%s" % wolf)

        # All alive players can be targeted, if werewolves wanna kill one of them, fine.
        list_potential_target = self.get_players_with_status()
        print('\tSelect werewolves target')
        target = chose_from_list(list_potential_target)
        self._werewolf_kill(target)

    def _werewolf_kill(self, target):
        current_round = self.round
        self._set_werewolf_victim(target)

    ## Witch turn
    def _run_witch(self):
        """
        Run the turn of the witch. Tell her who is the victim and, if the witch still has potions, allows
        her to use any of them
        :return:
        """
        witch = self.get_players_with_role('Witch')
        if len(witch) == 0:
            print('\tWitch seems to be dead')
            return 0
        print('\tThe witch is %s' % witch[0])

        current_round = self.round
        try:
            victim = self.werewolves_victims[current_round]
        except KeyError:
            print('\tSeems like werewolves turn has not happened yet')
            return 1
        print('\tThe victim tonight was %s' % victim)

        potions_left = self._get_witch_potions()
        if len(potions_left) == 0:
            print("\tNo potion left for the witch")
            return 0

        print("\tPotions left for the witch: ", potions_left)
        for potion in potions_left:
            choice_0 = input('\tDoes the witch want to use potion %s? [Y/N]:\t' % potion)
            if choice_0 == 'Y':
                self._use_witch_potion(potion)
            else:
                print('\tThe witch decided to not use the potion %s' % potion)

    def _witch_revive(self):
        # Get the last victim from the werewolves
        victim = self._get_werewolf_victim()
        # Revive the victim
        print('\tThe witch uses her potion to revive %s' % victim)
        self._set_player_status(victim, 'Alive')
        # Log action
        self.witch_target_potion['life'] = victim
        # Potion has been used
        self.witch_potions['life'] = 0

    def _witch_kill(self):
        print('\tWitch can choose from the list of potential victims')
        list_alive_players = self.get_players_with_status()
        target = chose_from_list(list_alive_players)
        # Kill target
        self._set_player_status(target)
        # Log action
        self.witch_target_potion['death'] = target
        # Potion has been used
        self.witch_potions['death'] = 0
        # Print summary
        role_victim = self.get_player_role(target)
        print('\tWitch killed %s who was' % (target, role_victim))

    def _use_witch_potion(self, potion):
        if potion == 'life':
            self._witch_revive()

        elif potion == 'death':
            self._witch_kill()

        else:
            print('\tNot recognized potion')

    # Internal checks
    def _check_all_role_assigned(self):
        list_available_roles = self.list_available_roles
        if len(list_available_roles) == 0:
            return True
        else:
            return False

    def _vote_is_consensus(self, round_value=None, iteration=None):
        """
        For a given iteration of votes, check if a consensus (majority) was reached
        :param round_value: default current one
        :param iteration: default current one
        :return: True or False
        """
        t_round, t_iteration = self._get_round_iteration(round_value, iteration)
        voted = self._get_voted(t_round, t_iteration)
        voted_counts = list(voted.values())
        # Get the maximum number of votes a player got against them and check if they are the only one
        max_counts = max(voted_counts)
        number_max_counts = voted_counts.count(max_counts)
        return number_max_counts == 1

    # External getters
    def get_player_role(self, name):
        """
        For a given name (if present in the players) return the player role
        :param name:
        :return role:
        """
        if name not in list(self.players.keys()):
            print('%s not present in the players')
            return None
        else:
            return self.players[name][0]

    def get_player_status(self, name):
        """
        For a given name (if present in the players) return the player status (Alive or Dead)
        :param name:
        :return status:
        """
        if name not in list(self.players.keys()):
            print('%s not present in the players' % name)
            return None
        else:
            return self.players[name][1]

    def get_players_with_status(self, status='Alive'):
        """
        Return the list of still alive players
        :param status: default is Alive, other option is Dead
        :return list player names:
        """
        if status not in ['Alive', 'Dead']:
            print('Status has to be Dead or Alive')
            return []
        players = self.players
        player_names = players.keys()
        players_status = [_ for _ in player_names if players[_][1] == status]
        return players_status

    def get_players_with_role(self, rolename, status='Alive'):
        """
        This function return the list of players with a given role
        :param rolename: Name of the role [werewvolf, witch, fortunteller, little-girl, peasant]
        :param status: default is alive, if none is given, all the players (including dead ones) are returned
        :return list of players with given role (and status):
        """
        players = self.players
        try:
            players_with_role = [_ for _ in players.keys() if players[_][0] == rolename]
        except KeyError:
            print("There is no player with role %s" % rolename)
            return None
        if status is None:
            return players_with_role
        else:
            players_with_role_status = [_ for _ in players_with_role if players[_][1] == status]
            return players_with_role_status

    def get_remaining_roles(self):
        list_alive_players = self.get_players_with_status()
        list_alive_roles = [self.get_player_role(_) for _ in list_alive_players]
        alive_role_counts = collections.Counter(list_alive_roles)
        return alive_role_counts

    # External actions

    def enter_vote(self, name, round_value=None, value_iteration=None):
        """
        Wrapper for set_vote method, asking and checking inputs
        :param name: voting person
        :param round_value: default current round
        :param iteration_value: default current iteration
        :return:
        """
        list_possible_players = self.get_players_with_status()
        list_possible_players.remove(name)  # cannot vote against yourself
        chosen_player = chose_from_list(list_possible_players)
        self.set_vote(name, chosen_player, round_value, value_iteration)

    def enter_votes(self):
        """
        This function allows you to enter a vote for the current round.
        Automatically create new entry for each iteration, in case several votes are needed for a given round
        :return:
        """
        current_round = self.round
        if current_round in self.votes.keys():
            iteration = max(self.votes[current_round].keys())
            iteration += 1
            self.votes[current_round][iteration] = {}
        else:
            self.votes[current_round] = {}
            iteration = 0
            self.votes[current_round][iteration] = {}

        list_player_names = self.get_players_with_status()

        print('\n*****************\nStarting iteration %d for round %d' % (current_round, iteration))
        print('Here is the current status of the players:')
        self.print_player_statuses()
        for name in list_player_names:
            print('\nEnter vote for \033[1m%s\033[0m' % name)
            self.enter_vote(name, current_round, iteration)

    def set_vote(self, name, vote, round_value=None, iteration=None):
        """
        This function allows, if it already exists, to change the vote of a given player.
        Can be for a given iteration and round, if not given, take the latest one.
        :param name: Voting player name
        :param vote: Voted player name
        :param round_value: round, default is latest
        :param iteration: iteration, default is latest of the selected round
        :return:
        """
        t_round, t_iteration = self._get_round_iteration(round_value, iteration)
        try:
            self.votes[t_round][t_iteration][name] = vote
        except KeyError:
            print('Cannot set vote for round %d, iteration %d for %s. No such entry' % (round_value, iteration, name))

    def delete_vote_iteration(self, round_value=None, iteration=None):
        """
        Delete a given vote iteration. By default latest one
        :param round_value: round, if not given latest one
        :param iteration: iteration, if not given, round latest one
        :return:
        """
        t_round, t_iteration = self._get_round_iteration(round_value, iteration)
        self.votes[t_round].pop(t_iteration)

    def delete_vote_round(self, round_value=None):
        """
        Delete all vote from a given round, default is current. Careful, this will potentially mess up the counting
        :param round_value: default latest one
        :return:
        """
        t_round, t_iteration = self._get_round_iteration(round_value)

        self.votes.pop(t_round)

    # Print functions

    def print_witch_targets(self):
        for key, value in self.witch_target_potion.items():
            if value is not None:
                print('Witch used %s on %s' % (key, value))

    def print_vote_iteration(self, round_value=None, iteration=None):
        """
        Print the different votes for a given round , iteration
        :param round_value: default is current one
        :param iteration: default is current one
        :return:
        """

        t_round, t_iteration = self._get_round_iteration(round_value, iteration)
        votes = self.votes[t_round][t_iteration]
        # Ensure that we always print the remaining players in same order
        names = list(votes.keys())
        names.sort()
        print('\tList of votes for round %d, iteration %d' % (t_round, t_iteration))
        for name in names:
            vote = votes[name]
            name_f = format_string(name, 10)
            vote_f = format_string(vote, 10)
            print('\t\t%s voted against %s' % (name_f, vote_f))

    def print_vote_round(self, round_value=None):
        """
        This function print all the vote for a given round (current round by default).
        :param round_value:
        :return:
        """
        t_round, t_iteration = self._get_round_iteration(round_value)

        list_iterations = list(self.votes[t_round].keys())
        list_iterations.sort()
        for iteration in list_iterations:
            self.print_vote_iteration(t_round, iteration)

    def print_player(self, name):
        """
        For a given name (if present in the players) print player role and status
        :param name:
        :return:
        """
        if name not in list(self.players.keys()):
            print('%s not present in the players')
            return None

        role = self.get_player_role(name)
        status = self.get_player_status(name)
        name_format = format_string(name, 10)
        if role is not None:
            if len(role) >= 8:
                print('\t%s\t%s\t%s' % (name_format, role, status))
            else:
                print('\t%s\t%s\t\t%s' % (name_format, role, status))

    def print_players(self):
        """
        Print all current players role and status
        :return:
        """
        print('List of current players, their roles and status')
        for name in list(self.players.keys()):
            self.print_player(name)

    def print_player_status(self, name):
        """
        For a given name (if present in the players) print player status
        :param name:
        :return:
        """
        if name not in list(self.players.keys()):
            print('%s not present in the players')
            return None
        status = self.players[name][1]
        print('\t%s is %s' % (name, status))

    def print_player_statuses(self):
        """
        print all players statuses, starting with dead ones.
        :return:
        """
        players = self.players
        players_names = self._get_player_names()
        dead_players = [_ for _ in players_names if players[_][1] == 'Dead']
        dead_player_roles = [self.get_player_role(_) for _ in dead_players]
        alive_players = [_ for _ in players_names if players[_][1] == 'Alive']
        print('\tDead players:')
        for player_name, player_role in zip(dead_players, dead_player_roles):
            print('\t\t%s\t%s' % (player_name, player_role))
        print('\tAlive players')
        for player_name in alive_players:
            print('\t\t%s' % player_name)

    def print_remaining_roles(self):
        list_alive_players = self.get_players_with_status()
        list_alive_roles = [self.get_player_role(_) for _ in list_alive_players]
        alive_role_counts = collections.Counter(list_alive_roles)
        for role, count in alive_role_counts.items():
            print('\t%d %s' % (count, role))

    # Main functions
    def start_game(self):
        """
        This function start the game by adding players and automatically assign them random roles
        """
        self._set_number_players()
        self._set_available_roles()

        logger.debug('Initial number of roles is %d' % (len(self.list_available_roles)))

        while not self._check_all_role_assigned():
            print('%d more players needed' % (self._get_number_available_roles()))
            print('Choose one option below:')
            print('\t[1] Add one player')
            print('\t[2] Add several players')
            print('\t[3] Quit')
            choice = input('Your choice:\t')
            try:
                choice_number = int(choice)
            except ValueError:
                print('Expected an integer')
                continue
            if choice_number not in [1, 2, 3]:
                print('Value should be in [1,2,3]')
                continue

            if choice_number == 3:
                print('Bye')
                sys.exit(0)
            if choice_number == 2:
                self._add_players()
            if choice_number == 1:
                self._add_player()

        # Print the role assignment
        self.print_players()

    def play_round(self):
        """
        To play one new round of werewolves.
        Play first the round of werewolves, then the fortune teller and then the witch.
        End by a round of votes for players still alive and keep on voting until a consensus is reached
        :return:
        """
        self.round += 1
        print('\n\n**************************')
        print('Starting round %d' % self.round)
        print('**************************')

        # Check if there are still werewolves alive
        remaining_roles = self.get_remaining_roles()
        if 'Werewolf' not in remaining_roles.keys():
            print('No more werewolves alive. Peasants won the game')
            return 0

        # Print the remaining roles in the game
        self.print_player_statuses()
        print('\nRemaining roles')
        self.print_remaining_roles()

        print('\nStarting fortuneteller round')
        input('Type enter to continue\n')
        self._run_fortuneteller()
        print('\nStarting werewolves round')
        input('Type enter to continue\n')
        self._run_werewolves()
        print('\nStarting witch round\n')
        input('Type enter to continue\n')
        self._run_witch()

        # Start a vote, keep voting until majority has voted
        # DOES NOT SUPPORT MAYOR
        consensus = 0
        while consensus == 0:
            self.enter_votes()
            consensus = self._vote_is_consensus()
            if consensus == 0:
                print('\n****** \033[1mNo consensus was found, another voting round is needed\033[0m')

        voted_name, voted_count = self._get_voted_name()
        voted_role = self.get_player_role(voted_name)
        print('%s voted out with %d votes, who was %s' % (voted_name[0], voted_count, voted_role))
        self._set_player_status(voted_name[0])
