import importlib
import GameInitiator
import logging

logger = logging.getLogger(__name__)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stdout_handler = logging.StreamHandler()
stdout_handler.setFormatter(formatter)
logger.addHandler(stdout_handler)
logger.setLevel(logging.DEBUG)
libs_logger = logging.getLogger('GameInitiator')
libs_logger.addHandler(stdout_handler)

importlib.reload(GameInitiator)

game_1 = GameInitiator.GameInitiator()
game_1.start()
#9
#2
#Alice Bob Charles Daisy Edwin Frank Gabriela Hector Irene
game_1.start_new_round()
